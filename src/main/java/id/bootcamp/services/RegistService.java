package id.bootcamp.services;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;
import id.bootcamp.models.M_User;
import id.bootcamp.repositories.RegistrationRepository;

@Service
@Transactional
public class RegistService {
	@Autowired
	private RegistrationRepository rr;
	
	public List<M_User> getAllData(){
		return rr.findAll();
	}
	//searchEmail
	public List<M_User> cariEmail(@Param ("cari") String cari){
		return rr.cariEmail(cari);
	}
	public void insertData(M_User mu) {
		rr.save(mu);
	}
}
