package id.bootcamp.services;

import java.util.List;

import org.springframework.data.repository.query.Param;

import id.bootcamp.models.M_Specialization;


public interface SpecializationServiceImplements {
	public List<M_Specialization> getAllSpecialization();
	public M_Specialization getSpecializationById(Long id);
	public List<M_Specialization> cariName(String cari);
	public List<M_Specialization> searchSpecialist(@Param("textsearch") String textsearch);
	public void tambahData(M_Specialization ms);
	public void editSpesialization(M_Specialization ms);
	public void deleteData(Long id);
	
}
