package id.bootcamp.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import id.bootcamp.models.M_Specialization;
import id.bootcamp.repositories.SpecializationRepository;


@Transactional
public class SpecializationService implements SpecializationServiceImplements{
	
	private SpecializationRepository sr;
	
	public SpecializationService(SpecializationRepository sr) {
		this.sr = sr;
	}
	
	@Override
	public List<M_Specialization> getAllSpecialization(){
		return sr.findAll();
	}
	@Override
	public M_Specialization getSpecializationById(Long id) {
		// TODO Auto-generated method stub
		return sr.findById(id).get();
	}
	//searchByName
	@Override
	public List<M_Specialization> cariName(String cari){
		return sr.cariName(cari);
	}
	@Override
	public List<M_Specialization> searchSpecialist(@Param("textsearch") String textsearch){
		return sr.searchSpecialist(textsearch);
}
	@Override
	public void tambahData(M_Specialization ms) {
		sr.save(ms);
	}
	
	@Override
	//editdata
	public void editSpesialization(M_Specialization ms) {
		sr.save(ms);
	}
	//delete
	@Override
	public void deleteData(Long id) {
		sr.deleteById(id);
	}
}
