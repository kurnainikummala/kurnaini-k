package id.bootcamp.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@MappedSuperclass
public class BaseProperties {
	
	@OneToOne
	@JoinColumn(name = "created_by", insertable = false, updatable = false)
	private M_User createByUser;
	
	@Column(nullable = false)
	private Long created_by;
	
	@Column(nullable = false)
	private Date created_on;
	
	@OneToOne
	@JoinColumn(name = "modified_by", insertable = false, updatable = false)
	private M_User modifiedByUser;
	
	private Long modified_by;
	
	private Date modified_on;
	
	@OneToOne
	@JoinColumn(name = "deleted_by", insertable = false, updatable = false)
	private M_User deletedByUser;
	
	private Long deleted_by;
	
	private Date deleted_on;
	
	@Column(nullable = false)
	private Boolean is_deleted = false;

	public M_User getCreateByUser() {
		return createByUser;
	}

	public void setCreateByUser(M_User createByUser) {
		this.createByUser = createByUser;
	}

	public Long getCreated_by() {
		return created_by;
	}

	public void setCreate_by(Long create_by) {
		this.created_by = create_by;
	}

	public Date getCreated_on() {
		return created_on;
	}

	public void setCreate_on(Date create_on) {
		this.created_on = create_on;
	}

	public M_User getModifiedByUser() {
		return modifiedByUser;
	}

	public void setModifiedByUser(M_User modifiedByUser) {
		this.modifiedByUser = modifiedByUser;
	}

	public Long getModified_by() {
		return modified_by;
	}

	public void setModified_by(Long modified_by) {
		this.modified_by = modified_by;
	}

	public Date getModified_on() {
		return modified_on;
	}

	public void setModified_on(Date modified_on) {
		this.modified_on = modified_on;
	}

	public M_User getDeletedByUser() {
		return deletedByUser;
	}

	public void setDeletedByUser(M_User deletedByUser) {
		this.deletedByUser = deletedByUser;
	}

	public Long getDeleted_by() {
		return deleted_by;
	}

	public void setDeleted_by(Long deleted_by) {
		this.deleted_by = deleted_by;
	}

	public Date getDeleted_on() {
		return deleted_on;
	}

	public void setDeleted_on(Date deleted_on) {
		this.deleted_on = deleted_on;
	}

	public Boolean getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(Boolean is_deleted) {
		this.is_deleted = is_deleted;
	}
	
	
	
}
