package id.bootcamp.configs;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import id.bootcamp.repositories.SpecializationRepository;
import id.bootcamp.services.SpecializationService;
import id.bootcamp.services.SpecializationServiceImplements;

@Configuration
public class IocBeanConfiguration {
	@Bean
	SpecializationServiceImplements specializationServiceImplements(SpecializationRepository sr) {
		return new SpecializationService(sr);
	}
}
