package id.bootcamp.controllers;


import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import id.bootcamp.models.M_User;
import id.bootcamp.services.RegistService;

@RestController
@RequestMapping("register")
public class RegisterRestController {
	
	@Autowired
	private RegistService rs;
	
	@GetMapping("getAllData")
	public List<M_User> getAllData(){
		return rs.getAllData();
	}
	@GetMapping("getEmail")
	public ResponseEntity<List<M_User>> getAllEmail(@RequestParam("cari") String cari){
		try {
			List<M_User> m_user = this.rs.cariEmail(cari);
			return new ResponseEntity<>(m_user, HttpStatus.OK);
			
		}catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("insertData")
	public String insertData(@RequestBody M_User mu) {
		//cek email
		List<M_User> emailSama = rs.cariEmail(mu.getEmail());
		if (emailSama.size() > 0) {
			return "Email sudah dipakai";
		}
		rs.insertData(mu);
		
		return "ok";
	}
}
