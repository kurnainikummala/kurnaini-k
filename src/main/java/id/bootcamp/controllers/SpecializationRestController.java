package id.bootcamp.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.catalina.connector.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.bootcamp.models.M_Specialization;
import id.bootcamp.services.SpecializationService;
import id.bootcamp.services.SpecializationServiceImplements;

@RestController
@RequestMapping("specializationApi")
public class SpecializationRestController {
	
	
	private SpecializationServiceImplements ss;
	
	public SpecializationRestController(SpecializationServiceImplements ss) {
		this.ss = ss;
	}
	
	@GetMapping("getAllSpecialization")
	public List<M_Specialization> getAllSpecialization(){
		//java stream 
		List<M_Specialization> specializations = ss.getAllSpecialization();
		List<M_Specialization> sortedData = specializations.stream()
				.sorted((id1, id2) -> id1.getName().compareTo(id2.getName()))
				.filter(data -> data.getName().equals("Spesialis Anak"))
				.collect(Collectors.toList());
		
		return sortedData;
	}
	
	@GetMapping("getSpecialization/{id}")
	public M_Specialization getSpecializationById(@PathVariable("id") Long id) {
		return ss.getSpecializationById(id);
	}
	
	@GetMapping("getSpecialization/search")
	public ResponseEntity<List<M_Specialization>> getAllspecializatios(@RequestParam("textsearch") String textsearch){
		try {
			List<M_Specialization> m_specialization = this.ss.searchSpecialist(textsearch);
			return new ResponseEntity<>(m_specialization, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		
	}
	
	@PostMapping("tambahData")
	public String tambahData(@RequestBody M_Specialization ms) {
		List<M_Specialization> nameSama = ss.cariName(ms.getName());
		if (nameSama.size() > 0 ) {
			return "Specialization sudah dipakai";
		}
		ss.tambahData(ms);
		return "ok";
	}
	@PutMapping("editData")
	public String editData(@RequestBody M_Specialization ms) {
		M_Specialization dataSebelum=ss.getSpecializationById(ms.getId());
		String nameSebelum = dataSebelum.getName();
		if(!ms.getName().equals(nameSebelum)) {
			List<M_Specialization> mengandungName = ss.cariName(ms.getName());
			if(mengandungName.size() > 0) {
				return "Spesialisasi sudah dipakai";
			}
		}
		ss.editSpesialization(ms);
		return"ok";
	}
	@DeleteMapping("deleteData/{id}")
	public String deleteData(@PathVariable("id") Long id) {
		ss.deleteData(id);
		return "ok";
	}
	 
}
