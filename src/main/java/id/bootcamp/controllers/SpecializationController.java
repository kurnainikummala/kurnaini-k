package id.bootcamp.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("specialization")
public class SpecializationController {
	@RequestMapping("")
	public String specialization() {
		return "specialization/specialization";
	}
	@RequestMapping("tambahData")
	public String tambahData() {
		return "specialization/tambahData";
	}
	
}
