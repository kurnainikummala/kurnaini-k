package id.bootcamp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.bootcamp.models.M_Specialization;
import id.bootcamp.models.M_User;

public interface RegistrationRepository extends JpaRepository<M_User, Long>{
	@Query(value = "SELECT * FROM m_user WHERE is_deleted = false and email = :search", nativeQuery = true)
	List<M_User> cariEmail(String search);
	
	@Query(value="SELECT * FROM m_user mu WHERE mu.email ILIKE %:cari%", nativeQuery=true)
	List<M_Specialization> searchSpecialist(@Param("cari") String cari);
}
