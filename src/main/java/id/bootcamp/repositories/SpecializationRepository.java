package id.bootcamp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.bootcamp.models.M_Specialization;

public interface SpecializationRepository extends JpaRepository<M_Specialization, Long>{
	@Query(value = "SELECT * FROM m_specialization WHERE is_deleted = false and name = :cari", nativeQuery = true)
	List<M_Specialization> cariName(String cari);
	
	
	@Query(value="SELECT * FROM m_specialization ms WHERE ms.name ILIKE %:textsearch%", nativeQuery=true)
	List<M_Specialization> searchSpecialist(@Param("textsearch") String textsearch);

}
